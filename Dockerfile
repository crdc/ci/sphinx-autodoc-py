FROM debian:buster

# basic requirements
RUN apt update
RUN apt install -y git
RUN apt install -y python3-dev
RUN apt install -y python3-pip
RUN apt install -y make

# update pip; refresh bash to expose newer version of pip3
RUN pip3 install --upgrade pip
RUN exec bash

# install python requirements
RUN pip3 install numpy
RUN pip3 install numpydoc
RUN pip3 install matplotlib
RUN pip3 install scipy
RUN pip3 install sklearn
RUN pip3 install pywavelets
RUN pip3 install sphinx

# for testing outside of gitlab
# executes the runner tasks
#RUN GIT_SSL_NO_VERIFY=true git clone https://gitlab.coanda.local/tdepe/mva.git
#WORKDIR "/mva/doc/"
#RUN make html
#RUN cp -r build/html/ ../public/
