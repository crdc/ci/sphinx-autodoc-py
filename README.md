This is a Dockerfile to generate a base image for performing API documentation
generation for Python projects using Sphinx. There is a docker image that can
be used in your `.gitlab-ci.yml` files in the project registry:

```
registry.gitlab.com/crdc/sphinx-autodoc-py/sphinx-autodoc-py:v1
```
# Sample CI

A sample `.gitlab-ci.yml` that you can use in your projects is shown below.
The pipeline utilizes the image hosted at this repository registry as a base. The
CI file assumes that the Sphinx source documentation is located in a directory
called `doc/` in the project base path.

`.gitlab-ci.yml`
```
image: registry.gitlab.com/crdc/docker/sphinx-autodoc-py/sphinx-autodoc-py:v1

pages:
    variables:
        GIT_SSL_NO_VERIFY: "true"
    stage: deploy
    script:
        - cd doc/
        - make html
        - mv build/html/ ../public
    artifacts:
        paths:
        - public
```

For more information on Continuous Integration in Gitlab check the documentation
on [Gitlab CI/CD](https://gitlab.com/help/ci/quick_start/README).

# Docker

Some supplementary information on using Docker and building/pushing images to
the registry.

## Docker Installation

Follow the instructions at https://docs.docker.com/install/.

## Local Testing

To build your docker image locally, make sure that `dockerd` is running.

Build docker image with a tag; this assumes you are in the path with the
`Dockerfile` (Note the `.` indicating the current directory).
```bash
docker build -t sphinx-autodoc-py:v1 .
```
Run interactively in a local container using this image.
```bash
docker run -i -t sphinx-autodoc-py:v1
```

You can get out of the container by typing `exit` in the shell.


## Docker Container Registry

Complete instructions here: https://docs.gitlab.com/ee/user/packages/container_registry/

### Personal Access Token

Get a personal access token by following instructions here:
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

You want **api** scope.

### Push to Registry

run
```bash
docker login registry.gitlab.com -u <username> -p <token>
```

Using your Gitlab username and the access token provided from your settings page.

Build and push to docker registry:
```bash
docker build -t registry.gitlab.com/crdc/sphinx-autodoc-py/sphinx-autodoc-py:v1 .
```

```bash
docker push registry.gitlab.com/crdc/sphinx-autodoc-py/sphinx-autodoc-py:v1
```
